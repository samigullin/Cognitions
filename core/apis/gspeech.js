// Libraries/modules
var speech = require('@google-cloud/speech');
// Local libraries
eval(fs.readFileSync("./../core/apis/audioconvert.js")+'');

var GSpeech = {
	recognize: function(voiceFilePath, callback) {
		// Converting
		var finalVoicePath;
		if(voiceFilePath.indexOf("wav") > -1)
			finalVoicePath = AudioConvert.wavToFlac(voiceFilePath);
		else if(voiceFilePath.indexOf("oga") > -1)
			finalVoicePath = AudioConvert.ogaToFlac(voiceFilePath);
		else if(voiceFilePath.indexOf("ogg") > -1)
			finalVoicePath = AudioConvert.oggToFlac(voiceFilePath);
		else {
			console.log("[!] Critical error! Voice file name contains unknown extension!");
			return false;
		}
		// Google speech recognize
		const client = new speech.SpeechClient({
			projectId: "cognitions-201018",
			keyFilename: path.resolve(__dirname + "/../data/secrets/cognitions-1ffadbbd9747.json")
		});
		const file = fs.readFileSync(finalVoicePath);
		const audioBytes = file.toString("base64");
		const audio = {
			content: audioBytes,
		};
		const config = {
			encoding: "LINEAR16",
			sampleRateHertz: 16000,
			languageCode: "ru-RU",
		};
		const request = {
			audio: audio,
			config: config,
		};
		client
			.recognize(request)
			.then(function(data) {
				const response = data[0];
				const transcription = response.results
					.map(result => result.alternatives[0].transcript)
					.join('\n');
				console.log(`Google Speech result: ${transcription}`);
				callback(transcription);
			})
			.catch(err => {
				console.error("Google Speech error:", err);
				return false;
			});
	}
}