// Libraries/modules
var SyncRequest = require("sync-request");
// Local libraries
eval(fs.readFileSync("./../tools/tools.js")+'');
// Database files(e.g.: settings)
var AppSettings = require("./../database/settings.json");

var Aimee = {
	sendQuery: function(requestType, aimeeMethod, data) {
		// requestType: *POST* or *GET*
		// aimeeMethod: http api method(e.g.: /ai_config)
		// data: data dictionary, which is uploading to server(only for *POST*)
		if(data != null)
			data = "?" + Tools.keyValueToUrlQuery(data);
		else
			data = "";
		var result = SyncRequest(requestType, AppSettings.common.aimeeServer + "/" + aimeeMethod + "/" + data, {
			headers: {
				"Accept": "application/json",
				"X-CSRFToken": AppSettings.common.aimeeToken
			}
		}).getBody("utf-8");
		return result;
	},
}