var DataLoader = {
	// Server data
	recordedVoiceDataBlob: null,
	cardsData: null,

	// Data load methods
	loadAllCards: function() {
		var audioContentData, imageContentData, cardPaddingFix;
		$("#CardsList").html("");
		$(".NavbarCardsTitle").html("");
		for(var i = 0; i < this.cardsData.length; i++) {
			audioContentData = "";
			imageContentData = "";
			if(this.cardsData[i].audio != "")
				audioContentData = '<audio controls><source src="' + this.cardsData[i].audio + '" type="audio/ogg; codecs=vorbis"></audio>';
			if(this.cardsData[i].image != "")
				imageContentData = '<img class="card-img-top" src="' + this.cardsData[i].image + '" alt="Запись">';
			cardPaddingFix = "";
			$("#CardsList").append('<div id="OneCard' + i.toString() + '" class="card OneCard"><h3 class="OneCardTitle">' + this.cardsData[i].title + '</h3><div class="OneCardAudioContent">' + audioContentData + '</div><div class="OneCardImageContent">' + imageContentData + '</div><div class="card-body"><div class="OneCardText"><h5 class="card-title">' + this.cardsData[i].title + '</h5><p class="card-text">' + this.cardsData[i].text + '</p></div><button class="btn btn-primary OneCardMoreBtn">Показать текст</button></div></div>');
			$(".NavbarCardsTitle").append('<div class="NavbarOneCardTitle"><a href="#OneCard' + i.toString() + '" class="btn btn-success NavbarOneCardBtn">' + this.cardsData[i].title + '</button></div>');
		}
	}
}