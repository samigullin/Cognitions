// Main block activity
$("#AddNewNotePageBtn").click(function() {
	UI.switchBlock("TakeNote");
});
$("#ShowAllNotesPageBtn").click(function() {
	UI.switchBlock("Cards");
});

// Take note block activity
//
$("#TakeNewVoiceNoteBtn").click(function() {
	UI.showElem("TakeNewVoiceNote");
});
$("#SaveVoiceBtn").click(function() {
	saveAudio();
});
$("#SendNewVoiceNoteBtn").click(function() {
	API.sendNewVoiceNote();
});
//
$("#TakeNewImageNoteBtn").click(function() {
	UI.showElem("TakeNewImageNote");
});
$(document).on("change", "#NewImageNoteFile", function() {
	var input = $(this),
		numFiles = input.get(0).files ? input.get(0).files.length : 1,
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
	$("#NewImageNoteFileName").html($(this).val());
});
$("#SendNewImageNoteBtn").click(function() {
	API.sendNewImageNote();
});
//
$("#TakeNewTextNoteBtn").click(function() {
	UI.showElem("TakeNewTextNote");
});
$("#SendNewTextNoteBtn").click(function() {
	API.sendNewTextNote();
});
//
$("body").on("click", ".GoHomeBtn", function() {
	UI.showElem("TakeNoteMenu");
	UI.switchBlock("Main");
});

// Cards block activity
$("body").on("click", ".OneCardMoreBtn", function() {
	if($(this).parent().children('div:first').css("display") == "none") {
		$(this).html("Скрыть текст");
		$(this).parent().children('div:first').show("slow");
	}
	else {
		$(this).html("Показать текст");
		$(this).parent().children('div:first').hide("slow");
	}
});

$(document).on('click', 'a[href^="#"]', function (event) {
	$(".collapse").removeClass("show");
	event.preventDefault();
	$('html, body').animate({
		scrollTop: $($.attr(this, 'href')).offset().top - 70
	}, 500);
});