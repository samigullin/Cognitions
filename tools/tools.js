var Tools = {
	keyValueToUrlQuery: function(obj) {
		// input(json dictionary): {foo: "bar"}
		// output(http url query string): foo=bar
		var str = [];
		for (var p in obj)
			if (obj.hasOwnProperty(p)) {
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			}
		return str.join("&");
	}
}